package com.audit.auditdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuditDemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(AuditDemoApplication.class, args);
	}

}
