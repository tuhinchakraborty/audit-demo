package com.audit.auditdemo.controller;

import com.audit.auditdemo.model.Persona;
import com.audit.auditdemo.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class PersonaController {

    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("/save/{name}")
    public String save(@PathVariable("name") String name) {
        Persona persona = new Persona();
        persona.setName(name);
        personaRepository.save(persona);
        Persona savedPersona = personaRepository.getByName(persona.getName());
        return savedPersona.getId().toString() + " " + savedPersona.getName() + " " + savedPersona.createdBy;
    }

    @PutMapping("/update/{userId}")
    public String update(@PathVariable("userId") Long userId, @RequestBody Persona updatedPersona) {
        Persona persona = personaRepository.findById(userId).get();
        persona.setName(updatedPersona.getName());
        personaRepository.save(persona);

        Persona savedPersona = personaRepository.getByName(persona.getName());
        return savedPersona.getId().toString() + " " + savedPersona.getName() + " " + savedPersona.createdBy;
    }
}
